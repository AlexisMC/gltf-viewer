#version 330

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec3 aNormal;
layout(location = 2) in vec2 aTexCoords;
layout(location = 3) in vec3 aTangent;
layout(location = 4) in vec3 aBitangent;

out vec3 vFragPos;
out vec3 vViewSpacePosition;
out vec3 vViewSpaceNormal;
out vec2 vTexCoords;
out vec3 vTangentLightPosition;
out vec3 vTangentViewPosition;
out vec3 vTangentFragPosition;

uniform mat4 uModelViewProjMatrix;// projection
uniform mat4 uModelViewMatrix;// view
uniform mat4 uModelMatrix;// model
uniform mat4 uNormalMatrix;
uniform vec3 uLightPosition;
uniform vec3 uViewPosition;

void main()
{
    vViewSpacePosition = vec3(uModelViewMatrix * vec4(aPosition, 1.0));// out
    vFragPos = vec3(uModelMatrix * vec4(aPosition, 1.0));
    vTexCoords = aTexCoords;// out

    vViewSpaceNormal = normalize(vec3(uNormalMatrix * vec4(aNormal, 0.)));// out

    vec3 T = normalize(vec3(uNormalMatrix * vec4(aTangent, 0.0)));
    vec3 N = normalize(vec3(uNormalMatrix * vec4(aNormal, 0.0)));
    // re-orthogonalize T with respect to N
    T = normalize(T - dot(T, N) * N);
    // then retrieve perpendicular vector B with the cross product of T and N
    //vec3 B = cross(N, T);
    vec3 B = normalize(vec3(uNormalMatrix * vec4(aBitangent, 0.0)));

    //vec3 lightPosition = uLightPosition * vec3(1., 1., -1.) + vec3(-1., -1., 0.);

    mat3 TBN = mat3(T, B, N);
    vTangentLightPosition = TBN * uLightPosition;// camera space
    vTangentViewPosition  = TBN * uViewPosition;// camera space
    vTangentFragPosition  = TBN * vViewSpacePosition;// camera space

    gl_Position =  uModelViewProjMatrix * vec4(aPosition, 1.0);
    //vFragPos = gl_Position.xyz;// out
}
#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;
in mat3 vTBN;

out vec3 fColor;

uniform sampler2D uNormalMap;
uniform vec3 uLightingDirection;
uniform vec3 uLightingIntensity;

#define PI 3.1415926538f

vec3 simpleBRDF() {
    return vec3(1.f / PI, 1.f / PI, 1.f / PI);
}

void main()
{
    vec3 normal = texture(uNormalMap, vTexCoords).rgb;
    normal = normal * 2.0 - 1.0;

    //mirrored fragment check
    vec3 tangent = vTBN[0];
    vec3 bitangent = vTBN[1];

    mat3 newTBN = vTBN;

    newTBN[0] = newTBN[0];
    newTBN[1] = newTBN[1];
    newTBN[2] = newTBN[2];
    normal = normalize(newTBN * normal);

    //on souhaite faire en sorte qu'il n'y ait pas d'effet mirroir, faire un dot sur la normale et la normale map
    //ne permet pas de sauver la mise

    //on visualise la normal map, on voit que les vecteurs rouges vert bleus sont visibles des deux cotés ce qui n est pas bon signe
    fColor = simpleBRDF()  * dot(normal, uLightingDirection);//dot(normal, uLightingDirection);//////normal //*uLightingDirection; //* uLightingDirection; //dot(normal, uLightingDirection);//
}
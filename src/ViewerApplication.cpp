#include "ViewerApplication.hpp"

#include <iostream>
#include <numeric>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/io.hpp>

#include "utils/gltf.hpp"
#include "utils/cameras.hpp"
#include "utils/images.hpp"

#include <stb_image_write.h>
#include <tiny_gltf.h>

#define VERTEX_ATTRIB_POSITION_IDX 0
#define VERTEX_ATTRIB_NORMAL_IDX 1
#define VERTEX_ATTRIB_TEXCOORD0_IDX 2
#define VERTEX_ATTRIB_TANGENT_IDX 3
#define VERTEX_ATTRIB_BITANGENT_IDX 4


void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
    if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
        glfwSetWindowShouldClose(window, 1);
    }
}

bool ViewerApplication::loadGltfFile(tinygltf::Model &model) {
    tinygltf::TinyGLTF loader;
    std::string error;
    std::string warning;
    bool load = loader.LoadASCIIFromFile(&model, &error, &warning, m_gltfFilePath.string());

    if (!warning.empty()) {
        std::cout << warning << std::endl;
    }

    if (!error.empty()) {
        std::cout << error << std::endl;
    }

    return load;
}

std::vector<GLuint> ViewerApplication::createBufferObjects(
        const tinygltf::Model &model) {
    std::vector<GLuint> bufferObjects(model.buffers.size(), 0);

    glGenBuffers(GLsizei(model.buffers.size()), bufferObjects.data());
    for (int i = 0; i < model.buffers.size(); i++) {
        glBindBuffer(GL_ARRAY_BUFFER, bufferObjects[i]);

        glBufferStorage(GL_ARRAY_BUFFER, model.buffers[i].data.size(), model.buffers[i].data.data(), 0);
    }
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return bufferObjects;
}

std::vector<glm::vec3> mixTangentsAndBiTangents(std::vector<glm::vec3> tangents, std::vector<glm::vec3> bitangents) {
    std::vector<glm::vec3> mixed;
    for (int i = 0; i < tangents.size(); i++) {
        mixed.push_back(tangents[i]);
        mixed.push_back(bitangents[i]);
    }
    return mixed;
}

void computeTangentAndBiTangent(std::vector<glm::vec3> triangleVertices, std::vector<glm::vec2> textureUVs,
                                glm::vec3 &tangent, glm::vec3 &bitangent) {
    /*std::cout<<"triangleVertices[0] = " <<triangleVertices[0]<<std::endl;
    std::cout<<"triangleVertices[1] = " <<triangleVertices[1]<<std::endl;
    std::cout<<"triangleVertices[2] = " <<triangleVertices[2]<<std::endl;*/

    glm::vec3 edge1 = triangleVertices[1] - triangleVertices[0];
    glm::vec3 edge2 = triangleVertices[2] - triangleVertices[0];
    glm::vec2 deltaUV1 = textureUVs[1] - textureUVs[0];
    glm::vec2 deltaUV2 = textureUVs[2] - textureUVs[0];

    float f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

    tangent.x = f * (deltaUV2.y * edge1.x - deltaUV1.y * edge2.x);
    tangent.y = f * (deltaUV2.y * edge1.y - deltaUV1.y * edge2.y);
    tangent.z = f * (deltaUV2.y * edge1.z - deltaUV1.y * edge2.z);

    bitangent.x = f * (-deltaUV2.x * edge1.x - deltaUV1.x * edge2.x);
    bitangent.y = f * (-deltaUV2.x * edge1.y - deltaUV1.x * edge2.y);
    bitangent.z = f * (-deltaUV2.x * edge1.z - deltaUV1.x * edge2.z);

    //std::cout<< "tangent = "<<tangent<<std::endl;
    //std::cout<< "bitangent = "<<bitangent<<std::endl;
}

void computeTangentsAndBiTangents(std::vector<glm::vec3> triangleVertices, std::vector<glm::vec2> textureUVs,
                                  std::vector<glm::vec3> &tangents, std::vector<glm::vec3> &bitangents) {
    // AJOUTER ASSERTIONS

    glm::vec3 currentTangent;
    glm::vec3 currentBiTangent;

    for (int verticesIdx = 0; verticesIdx < triangleVertices.size(); verticesIdx += 3) {
        //std::cout << "\nverticesIdx = "<<verticesIdx << std::endl;
        computeTangentAndBiTangent(
                {triangleVertices[verticesIdx], triangleVertices[verticesIdx + 1], triangleVertices[verticesIdx + 2]},
                {textureUVs[verticesIdx], textureUVs[verticesIdx + 1], textureUVs[verticesIdx + 2]},
                currentTangent,
                currentBiTangent
        );
        tangents.insert(tangents.end(), {currentTangent, currentTangent, currentTangent});
        bitangents.insert(bitangents.end(), {currentBiTangent, currentBiTangent, currentBiTangent});
    }
}

void getWorldAndTextureCoordinates(const tinygltf::Model &model, int meshIdx, int primitiveIdx,
                                   std::vector<glm::vec3> &worldCoordinates,
                                   std::vector<glm::vec2> &textureCoordinates) {
    std::vector<std::string> names = {"POSITION", "TEXCOORD_0"};
    std::vector<int> sizes = {3, 2};

    const auto primitive = model.meshes[meshIdx].primitives[primitiveIdx];
    for (int attributeIdx = 0; attributeIdx < names.size(); attributeIdx++) {
        const auto positionAttrIdxIt = primitive.attributes.find(names[attributeIdx]);
        if (positionAttrIdxIt == end(primitive.attributes)) {
            continue;
        }
        const auto &positionAccessor = model.accessors[(*positionAttrIdxIt).second];
        if (positionAccessor.type != sizes[attributeIdx]) {
            std::cerr << "Position accessor with type != VEC" + sizes[attributeIdx] << " , skipping"
                      << std::endl;
            continue;
        }
        const auto &positionBufferView = model.bufferViews[positionAccessor.bufferView];
        const auto byteOffset = positionAccessor.byteOffset + positionBufferView.byteOffset;
        const auto &positionBuffer = model.buffers[positionBufferView.buffer];
        const auto positionByteStride = positionBufferView.byteStride ? positionBufferView.byteStride :
                                        sizes[attributeIdx] * sizeof(float);

        if (primitive.indices >= 0) {
            const auto &indexAccessor = model.accessors[primitive.indices];
            const auto &indexBufferView = model.bufferViews[indexAccessor.bufferView];
            const auto indexByteOffset = indexAccessor.byteOffset + indexBufferView.byteOffset;
            const auto &indexBuffer = model.buffers[indexBufferView.buffer];
            auto indexByteStride = indexBufferView.byteStride;

            switch (indexAccessor.componentType) {
                default:
                    std::cerr << "Primitive index accessor with bad componentType " << indexAccessor.componentType
                              << ", skipping it." << std::endl;
                    continue;
                case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
                    indexByteStride = indexByteStride ? indexByteStride : sizeof(uint8_t);
                    break;
                case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
                    indexByteStride = indexByteStride ? indexByteStride : sizeof(uint16_t);
                    break;
                case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
                    indexByteStride = indexByteStride ? indexByteStride : sizeof(uint32_t);
                    break;
            }

            for (size_t i = 0; i < indexAccessor.count; ++i) {
                uint32_t index = 0;
                switch (indexAccessor.componentType) {
                    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE:
                        index = *((const uint8_t *) &indexBuffer.data[indexByteOffset + indexByteStride * i]);
                        break;
                    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT:
                        index = *((const uint16_t *) &indexBuffer.data[indexByteOffset + indexByteStride * i]);
                        break;
                    case TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT:
                        index = *((const uint32_t *) &indexBuffer.data[indexByteOffset + indexByteStride * i]);
                        break;
                }
                if (names[attributeIdx] == "POSITION") {
                    worldCoordinates.push_back(
                            *((const glm::vec3 *) &positionBuffer.data[byteOffset + positionByteStride * index]));
                } else if (names[attributeIdx] == "TEXCOORD_0") {
                    textureCoordinates.push_back(
                            *((const glm::vec2 *) &positionBuffer.data[byteOffset + positionByteStride * index]));
                }
            }
        } else {
            for (size_t i = 0; i < positionAccessor.count; ++i) {
                if (names[attributeIdx] == "POSITION") {
                    worldCoordinates.push_back(
                            *((const glm::vec3 *) &positionBuffer.data[byteOffset + positionByteStride * i]));
                } else if (names[attributeIdx] == "TEXCOORD_0") {
                    textureCoordinates.push_back(
                            *((const glm::vec2 *) &positionBuffer.data[byteOffset + positionByteStride * i]));
                }
            }
        }
    }
}

GLuint createNormalMapVBO(std::vector<glm::vec3> tangents, std::vector<glm::vec3> bitangents) {
    //assert sizes ==

    GLuint buffer;

    std::vector<glm::vec3> mixed = mixTangentsAndBiTangents(tangents, bitangents);
    mixed.insert(mixed.end(), bitangents.begin(), bitangents.end());

    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferStorage(GL_ARRAY_BUFFER, mixed.size() * sizeof(glm::vec3), mixed.data(), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return buffer;
}


std::vector<GLuint>
ViewerApplication::createVertexArrayObjects(const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects,
                                            std::vector<VaoRange> &meshIndexToVaoRange) {
    std::vector<GLuint> vertexArrayObjects;

    uint vaoSize = 0;
    uint vaoIndex = 0;

    uint meshPrimitivesCount = 0;

    //For each mesh of the model
    for (int meshIdx = 0; meshIdx < model.meshes.size(); meshIdx++) {
        //Create a VAO range to store VAO primitives
        const auto vaoOffset = vertexArrayObjects.size();
        vertexArrayObjects.resize(vaoOffset + model.meshes[meshIdx].primitives.size());
        meshIndexToVaoRange.push_back(VaoRange{(int) vaoOffset,
                                               (int) model.meshes[meshIdx].primitives.size()}); // Will be used during rendering

        //Generate a VAO for each primitive
        glGenVertexArrays(model.meshes[meshIdx].primitives.size(), &vertexArrayObjects[vaoOffset]);

        //For each primitive of the current mesh
        for (int primitiveIdx = 0; primitiveIdx < model.meshes[meshIdx].primitives.size(); primitiveIdx++) {

            glBindVertexArray(vertexArrayObjects[vaoOffset + primitiveIdx]);

            const auto primitive = model.meshes[meshIdx].primitives[primitiveIdx];

            { // Scope for position
                const auto iterator = primitive.attributes.find("POSITION");
                if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
                    std::cout << "positions found" << std::endl;
                    // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
                    const auto accessorIdx = (*iterator).second;
                    const auto &accessor = model.accessors[accessorIdx];
                    const auto &bufferView = model.bufferViews[accessor.bufferView];// TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
                    const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

                    const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

                    glEnableVertexAttribArray(
                            VERTEX_ATTRIB_POSITION_IDX);// TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
                    glBindBuffer(GL_ARRAY_BUFFER, bufferObject); // TODO Bind the buffer object to GL_ARRAY_BUFFER

                    // TODO Call glVertexAttribPointer with the correct arguments.
                    // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
                    // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
                    const auto byteOffset = accessor.byteOffset +
                                            bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
                    glVertexAttribPointer(VERTEX_ATTRIB_POSITION_IDX, accessor.type, accessor.componentType, GL_FALSE,
                                          GLsizei(bufferView.byteStride),
                                          (const GLvoid *) (accessor.byteOffset + bufferView.byteOffset));
                }
            }

            { // Scope for normals
                const auto iterator = primitive.attributes.find("NORMAL");
                if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
                    std::cout << "normals found" << std::endl;
                    // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
                    const auto accessorIdx = (*iterator).second;
                    const auto &accessor = model.accessors[accessorIdx];
                    const auto &bufferView = model.bufferViews[accessor.bufferView];// TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
                    const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

                    const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

                    glEnableVertexAttribArray(
                            VERTEX_ATTRIB_NORMAL_IDX);// TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
                    glBindBuffer(GL_ARRAY_BUFFER, bufferObject); // TODO Bind the buffer object to GL_ARRAY_BUFFER

                    // TODO Call glVertexAttribPointer with the correct arguments.
                    // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
                    // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
                    const auto byteOffset = accessor.byteOffset +
                                            bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
                    glVertexAttribPointer(VERTEX_ATTRIB_NORMAL_IDX, accessor.type, accessor.componentType, GL_FALSE,
                                          GLsizei(bufferView.byteStride),
                                          (const GLvoid *) (accessor.byteOffset + bufferView.byteOffset));
                }
            }

            { // Scope for texture coordinates
                const auto iterator = primitive.attributes.find("TEXCOORD_0");
                if (iterator != end(primitive.attributes)) { // If "POSITION" has been found in the map
                    std::cout << "texture coordinates found" << std::endl;
                    // (*iterator).first is the key "POSITION", (*iterator).second is the value, ie. the index of the accessor for this attribute
                    const auto accessorIdx = (*iterator).second;
                    const auto &accessor = model.accessors[accessorIdx];
                    const auto &bufferView = model.bufferViews[accessor.bufferView];// TODO get the correct tinygltf::BufferView from model.bufferViews. You need to use the accessor
                    const auto bufferIdx = bufferView.buffer; // TODO get the index of the buffer used by the bufferView (you need to use it)

                    const auto bufferObject = bufferObjects[bufferIdx]; // TODO get the correct buffer object from the buffer index

                    glEnableVertexAttribArray(
                            VERTEX_ATTRIB_TEXCOORD0_IDX);// TODO Enable the vertex attrib array corresponding to POSITION with glEnableVertexAttribArray (you need to use VERTEX_ATTRIB_POSITION_IDX which has to be defined at the top of the cpp file)
                    glBindBuffer(GL_ARRAY_BUFFER, bufferObject); // TODO Bind the buffer object to GL_ARRAY_BUFFER

                    // TODO Call glVertexAttribPointer with the correct arguments.
                    // Remember size is obtained with accessor.type, type is obtained with accessor.componentType.
                    // The stride is obtained in the bufferView, normalized is always GL_FALSE, and pointer is the byteOffset (don't forget the cast).
                    const auto byteOffset = accessor.byteOffset +
                                            bufferView.byteOffset; // TODO Compute the total byte offset using the accessor and the buffer view
                    glVertexAttribPointer(VERTEX_ATTRIB_TEXCOORD0_IDX, accessor.type, accessor.componentType, GL_FALSE,
                                          GLsizei(bufferView.byteStride),
                                          (const GLvoid *) (accessor.byteOffset + bufferView.byteOffset));
                }
            }

            {//normal map
                std::vector<glm::vec3> worldCoordinates;
                std::vector<glm::vec2> textureCoordinates;
                getWorldAndTextureCoordinates(model, meshIdx, primitiveIdx, worldCoordinates, textureCoordinates);

                std::vector<glm::vec3> tangents;
                std::vector<glm::vec3> bitangents;

                computeTangentsAndBiTangents(worldCoordinates, textureCoordinates, tangents, bitangents);

                GLuint vbo = createNormalMapVBO(tangents, bitangents);

                glEnableVertexAttribArray(VERTEX_ATTRIB_TANGENT_IDX);
                glEnableVertexAttribArray(VERTEX_ATTRIB_BITANGENT_IDX);
                glBindBuffer(GL_ARRAY_BUFFER, vbo);

                glVertexAttribPointer(VERTEX_ATTRIB_TANGENT_IDX, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                                      (const void *) 0);

                glVertexAttribPointer(VERTEX_ATTRIB_BITANGENT_IDX, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
                                      (const void *) (sizeof(float) * 3));

                glBindBuffer(GL_ARRAY_BUFFER, 0);
            }


            if (primitive.indices >= 0) {
                const auto accessorIdx = primitive.indices;
                const auto &accessor = model.accessors[accessorIdx];
                const auto &bufferView = model.bufferViews[accessor.bufferView];
                const auto bufferIdx = bufferView.buffer;

                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, bufferObjects[bufferIdx]);
            }

        }
    }
    glBindVertexArray(0);

    return vertexArrayObjects;
}

std::vector<GLuint> ViewerApplication::createTextureObjects(const tinygltf::Model &model) const {
    std::vector<GLuint> textureObjects(model.textures.size(), 0);

    tinygltf::Sampler defaultSampler;
    defaultSampler.minFilter = GL_LINEAR;
    defaultSampler.magFilter = GL_LINEAR;
    defaultSampler.wrapS = GL_REPEAT;
    defaultSampler.wrapT = GL_REPEAT;
    defaultSampler.wrapR = GL_REPEAT;

    glActiveTexture(GL_TEXTURE0);

    for (int textureIdx = 0; textureIdx < model.textures.size(); textureIdx++) {


        // Generate the texture object:
        glGenTextures(1, &textureObjects[textureIdx]);

        // Bind texture object to target GL_TEXTURE_2D:
        glBindTexture(GL_TEXTURE_2D, textureObjects[textureIdx]);

        const auto &texture = model.textures[textureIdx]; // get i-th texture
        assert(texture.source >= 0); // ensure a source image is present
        const auto &image = model.images[texture.source]; // get the image

        const auto &sampler = texture.sampler >= 0 ? model.samplers[texture.sampler] : defaultSampler;

        // fill the texture object with the data from the image
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.width, image.height, 0,
                     GL_RGBA, image.pixel_type, image.image.data());

        // Set sampling parameters:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        // Set wrapping parameters:
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);

        if (sampler.minFilter == GL_NEAREST_MIPMAP_NEAREST ||
            sampler.minFilter == GL_NEAREST_MIPMAP_LINEAR ||
            sampler.minFilter == GL_LINEAR_MIPMAP_NEAREST ||
            sampler.minFilter == GL_LINEAR_MIPMAP_LINEAR) {
            glGenerateMipmap(GL_TEXTURE_2D);
        }

    }

    glBindTexture(GL_TEXTURE_2D, 0);

    return textureObjects;
}


int ViewerApplication::run() {
    // Loader shaders
    const auto glslProgram =
            compileProgram({m_ShadersRootPath / m_vertexShader,
                            m_ShadersRootPath / m_fragmentShader});

    const auto modelViewProjMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewProjMatrix");
    const auto modelViewMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uModelViewMatrix");
    const auto normalMatrixLocation = glGetUniformLocation(glslProgram.glId(), "uNormalMatrix");

    const auto metallicRoughnessTextureUniform = glGetUniformLocation(glslProgram.glId(), "uMetallicRoughnessTexture");
    const auto metallicFactorUniform = glGetUniformLocation(glslProgram.glId(), "uMetallicFactor");
    const auto roughnessFactorUniform = glGetUniformLocation(glslProgram.glId(), "uRoughnessFactor");

    const auto lightingDirectionUniform = glGetUniformLocation(glslProgram.glId(), "uLightingDirection");
    const auto lightingIntensityUniform = glGetUniformLocation(glslProgram.glId(), "uLightingIntensity");
    const auto baseColorTextureUniform = glGetUniformLocation(glslProgram.glId(), "uBaseColorTexture");
    const auto baseColorFactorUniform = glGetUniformLocation(glslProgram.glId(), "uBaseColorFactor");

    const auto emissiveTextureUniform = glGetUniformLocation(glslProgram.glId(), "uEmissiveTexture");
    const auto emissiveFactorUniform = glGetUniformLocation(glslProgram.glId(), "uEmissiveFactor");

    const auto occlusionTextureUniform = glGetUniformLocation(glslProgram.glId(), "uOcclusionTexture");
    const auto occlusionStrengthUniform = glGetUniformLocation(glslProgram.glId(), "uOcclusionStrength");
    const auto applyOcclusionUniform = glGetUniformLocation(glslProgram.glId(), "uApplyOcclusion");

    const auto normalMapUniform = glGetUniformLocation(glslProgram.glId(), "uNormalMap");

    glm::vec3 lightDirection(1, 1, 1);
    glm::vec3 lightIntensity(1, 1, 1);
    bool lightFromCamera = false;
    bool applyOcclusion = true;

    tinygltf::Model model;
    // Loading the glTF file

    if (!loadGltfFile(model)) {
        return -1;
    }

    glm::vec3 bboxMin, bboxMax;
    computeSceneBounds(model, bboxMin, bboxMax);
    glm::vec3 bboxDiagonal = (bboxMax - bboxMin);

    // Build projection matrix
    auto maxDistance = glm::length(bboxDiagonal); //
    maxDistance = maxDistance > 0.f ? maxDistance : 100.f;
    const auto projMatrix =
            glm::perspective(70.f, float(m_nWindowWidth) / m_nWindowHeight,
                             0.001f * maxDistance, 1.5f * maxDistance);


    std::unique_ptr<CameraController> cameraController = std::make_unique<TrackballCameraController>(
            m_GLFWHandle.window(), 0.5f * maxDistance);

    if (m_hasUserCamera) {
        cameraController->setCamera(m_userCamera);
    } else {

        glm::vec3 up = glm::vec3(0, 1, 0);
        glm::vec3 center = (bboxMax + bboxMin) / 2.f;
        glm::vec3 eye;

        if (bboxDiagonal.z == 0) {
            eye = center + 2.f * glm::cross(bboxDiagonal, up);
        } else {
            eye = center + bboxDiagonal;
        }
        cameraController->setCamera(
                Camera{eye, center, up}
        );

    }

    // TODO Creation of Buffer Objects
    std::vector<VaoRange> meshToVertexArrays;
    std::vector<GLuint> bufferObjects = createBufferObjects(model);

    // TODO Creation of Vertex Array Objects

    std::vector<GLuint> vertexArrayObjects = createVertexArrayObjects(model, bufferObjects, meshToVertexArrays);

    // Creation of Texture Objects

    std::vector<GLuint> textureObjects = createTextureObjects(model);

    GLuint whiteTexture;

    float white[] = {1, 1, 1, 1};

    glGenTextures(1, &whiteTexture);
    glBindTexture(GL_TEXTURE_2D, whiteTexture);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
    glBindTexture(GL_TEXTURE_2D, 0);


    // Setup OpenGL state for rendering
    glEnable(GL_DEPTH_TEST);
    glslProgram.use();


    const auto bindMaterial = [&](const auto materialIndex) {
        if (materialIndex >= 0) {
            const auto &pbrMetallicRoughness = model.materials[materialIndex].pbrMetallicRoughness;


            if (baseColorFactorUniform >= 0) {
                glUniform4f(baseColorFactorUniform,
                            (float) pbrMetallicRoughness.baseColorFactor[0],
                            (float) pbrMetallicRoughness.baseColorFactor[1],
                            (float) pbrMetallicRoughness.baseColorFactor[2],
                            (float) pbrMetallicRoughness.baseColorFactor[3]);
            }
            if (metallicFactorUniform >= 0) {
                glUniform1f(
                        metallicFactorUniform, (float) pbrMetallicRoughness.metallicFactor);
            }
            if (roughnessFactorUniform >= 0) {
                glUniform1f(
                        roughnessFactorUniform, (float) pbrMetallicRoughness.roughnessFactor);
            }
            if (metallicRoughnessTextureUniform >= 0) {
                auto textureObject = 0u;
                if (pbrMetallicRoughness.metallicRoughnessTexture.index >= 0) {
                    const auto &texture =
                            model.textures[pbrMetallicRoughness.metallicRoughnessTexture.index];
                    if (texture.source >= 0) {
                        textureObject = textureObjects[texture.source];
                    }
                }

                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, textureObject);
                glUniform1i(metallicRoughnessTextureUniform, 1);
            }

            if (emissiveFactorUniform >= 0) {
                glUniform3f(emissiveFactorUniform, (float) model.materials[materialIndex].emissiveFactor[0],
                            (float) model.materials[materialIndex].emissiveFactor[1],
                            (float) model.materials[materialIndex].emissiveFactor[2]);
            }
            if (emissiveTextureUniform >= 0) {
                auto textureObject = 0u;
                if (model.materials[materialIndex].emissiveTexture.index >= 0) {
                    const auto &texture = model.textures[model.materials[materialIndex].emissiveTexture.index];
                    if (texture.source >= 0) {
                        textureObject = textureObjects[texture.source];
                    }
                }

                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_2D, textureObject);
                glUniform1i(emissiveTextureUniform, 2);
            }

            if (occlusionStrengthUniform >= 0) {
                glUniform1f(occlusionStrengthUniform, (float) model.materials[materialIndex].occlusionTexture.strength);
            }
            if (occlusionTextureUniform >= 0) {
                auto textureObject = whiteTexture;
                if (model.materials[materialIndex].occlusionTexture.index >= 0) {
                    const auto &texture = model.textures[model.materials[materialIndex].occlusionTexture.index];
                    if (texture.source >= 0) {
                        textureObject = textureObjects[texture.source];
                    }
                }

                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, textureObject);
                glUniform1i(occlusionTextureUniform, 3);
            }

            if (baseColorTextureUniform >= 0) {
                auto textureObject = whiteTexture;
                if (pbrMetallicRoughness.baseColorTexture.index >= 0) {
                    const auto texture = model.textures[pbrMetallicRoughness.baseColorTexture.index];
                    if (texture.source >= 0) {
                        textureObject = textureObjects[texture.source];
                    }
                }
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, textureObject);
                glUniform1i(baseColorTextureUniform, 0);
            }

            if (normalMapUniform >= 0) {
                const auto &normalTexture = model.materials[materialIndex].normalTexture;
                auto textureObject = whiteTexture;
                if (normalTexture.index >= 0) {
                    const auto texture = model.textures[normalTexture.index];
                    if (texture.source >= 0) {
                        textureObject = textureObjects[texture.source];
                    }
                }
                glActiveTexture(GL_TEXTURE4);
                glBindTexture(GL_TEXTURE_2D, textureObject);
                glUniform1i(normalMapUniform, 4);
            }
        } else {
            if (baseColorFactorUniform >= 0) {
                glUniform4f(baseColorFactorUniform, 1, 1, 1, 1);
            }

            if (baseColorTextureUniform >= 0) {
                glActiveTexture(GL_TEXTURE0);
                glBindTexture(GL_TEXTURE_2D, whiteTexture);
                glUniform1i(baseColorTextureUniform, 0);
            }

            if (metallicFactorUniform >= 0) {
                glUniform1f(metallicFactorUniform, 1.f);
            }
            if (roughnessFactorUniform >= 0) {
                glUniform1f(roughnessFactorUniform, 1.f);
            }
            if (metallicRoughnessTextureUniform >= 0) {
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, 0);
                glUniform1i(metallicRoughnessTextureUniform, 1);
            }
            if (emissiveFactorUniform >= 0) {
                glUniform3f(emissiveFactorUniform, 0.f, 0.f, 0.f);
            }
            if (emissiveTextureUniform >= 0) {
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_2D, 0);
                glUniform1i(emissiveTextureUniform, 2);
            }

            if (occlusionStrengthUniform >= 0) {
                glUniform1f(occlusionStrengthUniform, 0.f);
            }
            if (occlusionTextureUniform >= 0) {
                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, 0);
                glUniform1i(occlusionTextureUniform, 3);
            }
            if (normalMapUniform >= 0) {
                glActiveTexture(GL_TEXTURE4);
                glBindTexture(GL_TEXTURE_2D, 0);
                glUniform1i(normalMapUniform, 4);
            }
        }
    };


    // Lambda function to draw the scene
    const auto drawScene = [&](const Camera &camera) {
        glViewport(0, 0, m_nWindowWidth, m_nWindowHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        const auto viewMatrix = camera.getViewMatrix();

        // The recursive function that should draw a node
        // We use a std::function because a simple lambda cannot be recursive
        const std::function<void(int, const glm::mat4 &)> drawNode =

                [&](int nodeIdx, const glm::mat4 &parentMatrix) {
                    // TODO The drawNode function
                    auto node = model.nodes[nodeIdx];
                    glm::mat4 modelMatrix = getLocalToWorldMatrix(node, parentMatrix);
                    if (node.mesh >= 0) {
                        glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;
                        glm::mat4 modelViewProjectionMatrix = projMatrix * modelViewMatrix;
                        glm::mat4 normalMatrix = glm::transpose(glm::inverse(modelViewMatrix));

                        glUniformMatrix4fv(modelViewProjMatrixLocation, 1, GL_FALSE,
                                           glm::value_ptr(modelViewProjectionMatrix));
                        glUniformMatrix4fv(modelViewMatrixLocation, 1, GL_FALSE, glm::value_ptr(modelViewMatrix));
                        glUniformMatrix4fv(normalMatrixLocation, 1, GL_FALSE, glm::value_ptr(normalMatrix));

                        if (lightingIntensityUniform >= 0) {
                            glUniform3f(lightingIntensityUniform, lightIntensity.x, lightIntensity.y, lightIntensity.z);
                        }

                        if (applyOcclusionUniform >= 0) {
                            glUniform1i(applyOcclusionUniform, applyOcclusion);
                        }


                        if (lightingDirectionUniform >= 0) {
                            if (lightFromCamera) {
                                glUniform3f(lightingDirectionUniform, 0, 0, 1);
                            } else {
                                const auto lightDirectionInViewSpace = glm::normalize(
                                        glm::vec3(viewMatrix * glm::vec4(lightDirection, 0.)));
                                glUniform3f(lightingDirectionUniform, lightDirectionInViewSpace[0],
                                            lightDirectionInViewSpace[1], lightDirectionInViewSpace[2]);
                            }
                        }

                        tinygltf::Mesh mesh = model.meshes[node.mesh];
                        VaoRange vaoRange = meshToVertexArrays[node.mesh];

                        for (int primitiveIdx = 0; primitiveIdx < mesh.primitives.size(); primitiveIdx++) {
                            GLuint vao = vertexArrayObjects[vaoRange.begin + primitiveIdx];
                            tinygltf::Primitive primitive = mesh.primitives[primitiveIdx];

                            bindMaterial(primitive.material);
                            glBindVertexArray(vao);

                            if (primitive.indices >= 0) {
                                const auto primitiveAccessor = model.accessors[primitive.indices];
                                const auto bufferView = model.bufferViews[primitiveAccessor.bufferView];
                                glDrawElements(primitive.mode, primitiveAccessor.count, primitiveAccessor.componentType,
                                               (const GLvoid *) (primitiveAccessor.byteOffset + bufferView.byteOffset));

                            } else {
                                const auto accessorIdx = (*begin(primitive.attributes)).second;
                                const auto &accessor = model.accessors[accessorIdx];
                                glDrawArrays(primitive.mode, 0, accessor.count);
                            }
                        }
                    }

                    for (auto childNodeIdx : node.children) {
                        drawNode(childNodeIdx, modelMatrix);
                    }
                };

        // Draw the scene referenced by gltf file
        if (model.defaultScene >= 0) {
            for (auto nodeIdx : model.scenes[model.defaultScene].nodes) {
                drawNode(nodeIdx, glm::mat4(1));
            }
        }
    };

    if (!m_OutputPath.empty()) {
        std::vector<unsigned char> pixels(m_nWindowWidth * m_nWindowHeight * 3);
        renderToImage(m_nWindowWidth, m_nWindowHeight, 3, pixels.data(),
                      [&]() { drawScene(cameraController->getCamera()); });
        flipImageYAxis(m_nWindowWidth, m_nWindowHeight, 3, pixels.data());
        const auto strPath = m_OutputPath.string();
        stbi_write_png(strPath.c_str(), m_nWindowWidth, m_nWindowHeight, 3, pixels.data(), 0);
        return 0;
    }




    // Loop until the user closes the window
    for (auto iterationCount = 0u; !m_GLFWHandle.shouldClose();
         ++iterationCount) {
        const auto seconds = glfwGetTime();

        const auto camera = cameraController->getCamera();
        drawScene(camera);

        // GUI code:
        imguiNewFrame();

        {
            ImGui::Begin("GUI");
            ImGui::Text("Application average %.3f ms/frame (%.1f FPS)",
                        1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
            if (ImGui::CollapsingHeader("Camera", ImGuiTreeNodeFlags_DefaultOpen)) {
                ImGui::Text("eye: %.3f %.3f %.3f", camera.eye().x, camera.eye().y,
                            camera.eye().z);
                ImGui::Text("center: %.3f %.3f %.3f", camera.center().x,
                            camera.center().y, camera.center().z);
                ImGui::Text(
                        "up: %.3f %.3f %.3f", camera.up().x, camera.up().y, camera.up().z);

                ImGui::Text("front: %.3f %.3f %.3f", camera.front().x, camera.front().y,
                            camera.front().z);
                ImGui::Text("left: %.3f %.3f %.3f", camera.left().x, camera.left().y,
                            camera.left().z);

                if (ImGui::Button("CLI camera args to clipboard")) {
                    std::stringstream ss;
                    ss << "--lookat " << camera.eye().x << "," << camera.eye().y << ","
                       << camera.eye().z << "," << camera.center().x << ","
                       << camera.center().y << "," << camera.center().z << ","
                       << camera.up().x << "," << camera.up().y << "," << camera.up().z;
                    const auto str = ss.str();
                    glfwSetClipboardString(m_GLFWHandle.window(), str.c_str());
                }
                static int cameraControllerType = 0;
                const auto cameraControllerTypeChanged =
                        ImGui::RadioButton("Trackball", &cameraControllerType, 0) ||
                        ImGui::RadioButton("First Person", &cameraControllerType, 1);

                if (cameraControllerTypeChanged) {
                    const auto currentCamera = cameraController->getCamera();
                    if (cameraControllerType == 0) {
                        cameraController = std::make_unique<TrackballCameraController>(
                                m_GLFWHandle.window(), 0.5f * maxDistance);
                    } else {
                        cameraController = std::make_unique<FirstPersonCameraController>(
                                m_GLFWHandle.window(), 0.5f * maxDistance);
                    }
                    cameraController->setCamera(currentCamera);
                }
            }

            if (ImGui::CollapsingHeader("Light", ImGuiTreeNodeFlags_DefaultOpen)) {
                ImGui::Text("lightDirection: %.3f %.3f %.3f", lightDirection.x, lightDirection.y,
                            lightDirection.z);

                static float lightTheta = 0.f;
                static float lightPhi = 0.f;

                if (ImGui::SliderFloat("theta", &lightTheta, 0, glm::pi<float>()) ||
                    ImGui::SliderFloat("phi", &lightPhi, 0, 2.f * glm::pi<float>())) {
                    const auto sinPhi = glm::sin(lightPhi);
                    const auto cosPhi = glm::cos(lightPhi);
                    const auto sinTheta = glm::sin(lightTheta);
                    const auto cosTheta = glm::cos(lightTheta);
                    lightDirection =
                            glm::vec3(sinTheta * cosPhi, cosTheta, sinTheta * sinPhi);
                }

                static glm::vec3 lightColor(1.f, 1.f, 1.f);
                static float lightIntensityFactor = 1.f;

                if (ImGui::ColorEdit3("color", (float *) &lightColor) ||
                    ImGui::InputFloat("intensity", &lightIntensityFactor)) {
                    lightIntensity = lightColor * lightIntensityFactor;
                }
                ImGui::Checkbox("light from camera", &lightFromCamera);
                ImGui::Checkbox("apply occlusion", &applyOcclusion);
            }

            ImGui::End();
        }

        imguiRenderFrame();

        glfwPollEvents(); // Poll for and process events

        auto ellapsedTime = glfwGetTime() - seconds;
        auto guiHasFocus =
                ImGui::GetIO().WantCaptureMouse || ImGui::GetIO().WantCaptureKeyboard;
        if (!guiHasFocus) {
            cameraController->update(float(ellapsedTime));
        }

        m_GLFWHandle.swapBuffers(); // Swap front and back buffers
    }

    // TODO clean up allocated GL data

    return 0;
}

ViewerApplication::ViewerApplication(const fs::path &appPath, uint32_t width,
                                     uint32_t height, const fs::path &gltfFile,
                                     const std::vector<float> &lookatArgs, const std::string &vertexShader,
                                     const std::string &fragmentShader, const fs::path &output) :
        m_nWindowWidth(width),
        m_nWindowHeight(height),
        m_AppPath{appPath},
        m_AppName{m_AppPath.stem().string()},
        m_ImGuiIniFilename{m_AppName + ".imgui.ini"},
        m_ShadersRootPath{m_AppPath.parent_path() / "shaders"},
        m_gltfFilePath{gltfFile},
        m_OutputPath{output} {
    if (!lookatArgs.empty()) {
        m_hasUserCamera = true;
        m_userCamera =
                Camera{glm::vec3(lookatArgs[0], lookatArgs[1], lookatArgs[2]),
                       glm::vec3(lookatArgs[3], lookatArgs[4], lookatArgs[5]),
                       glm::vec3(lookatArgs[6], lookatArgs[7], lookatArgs[8])};
    }

    if (!vertexShader.empty()) {
        m_vertexShader = vertexShader;
    }

    if (!fragmentShader.empty()) {
        m_fragmentShader = fragmentShader;
    }

    ImGui::GetIO().IniFilename =
            m_ImGuiIniFilename.c_str(); // At exit, ImGUI will store its windows
    // positions in this file

    glfwSetKeyCallback(m_GLFWHandle.window(), keyCallback);

    printGLVersion();
}

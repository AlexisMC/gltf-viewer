#pragma once

#include "utils/GLFWHandle.hpp"
#include "utils/cameras.hpp"
#include "utils/filesystem.hpp"
#include "utils/shaders.hpp"

#include <tiny_gltf.h>

class ViewerApplication
{
public:
    ViewerApplication(const fs::path &appPath, uint32_t width, uint32_t height,
                      const fs::path &gltfFile, const std::vector<float> &lookatArgs,
                      const std::string &vertexShader, const std::string &fragmentShader,
                      const fs::path &output);

    int run();

private:
    // A range of indices in a vector containing Vertex Array Objects
    struct VaoRange
    {
        GLsizei begin; // Index of first element in vertexArrayObjects
        GLsizei count; // Number of elements in range
    };

    /**
     *
     * @param model le modèle tiny gltf
     * @return
     */
    bool loadGltfFile(tinygltf::Model &model);

    /**
     *
     * @param model le modèle tiny gltf
     * @return
     */
    std::vector<GLuint> createTextureObjects(const tinygltf::Model &model) const;

    /**
     *
     * @param model le modèle tiny gltf
     * @return
     */
    std::vector<GLuint> createBufferObjects(const tinygltf::Model &model) const;

    /**
     *
     * @param model le modèle tiny gltf
     * @param bufferObjects un buffer object
     * @param meshToVertexArrays
     * @return
     */
    std::vector<GLuint> createVertexArrayObjects(const tinygltf::Model &model,
                                                 const std::vector<GLuint> &bufferObjects,
                                                 std::vector<VaoRange> &meshToVertexArrays) const;

    /**
     *
     * @param model le modèle tiny gltf
     * @return
     */
    std::vector<GLuint> createBufferObjects( const tinygltf::Model &model);

    /**
     *
     * @param model le modèle tiny gltf
     * @param bufferObjects un buffer object duquel prendre les données
     * @param meshToVertexArrays
     * @return
     */
    std::vector<GLuint> createVertexArrayObjects(const tinygltf::Model &model, const std::vector<GLuint> &bufferObjects, std::vector<VaoRange> &meshToVertexArrays);

    GLsizei m_nWindowWidth = 1280;
    GLsizei m_nWindowHeight = 720;

    const fs::path m_AppPath;
    const std::string m_AppName;
    const fs::path m_ShadersRootPath;

    fs::path m_gltfFilePath;
    //std::string m_vertexShader = "forward.vs.glsl";
    //std::string m_fragmentShader = "pbr_directional_light.fs.glsl";
    std::string m_vertexShader = "normal_mapping.vs.glsl";
    std::string m_fragmentShader = "normal_mapping.fs.glsl";
    bool m_hasUserCamera = false;
    Camera m_userCamera;

    fs::path m_OutputPath;

    // Order is important here, see comment below
    const std::string m_ImGuiIniFilename;
    // Last to be initialized, first to be destroyed:
    GLFWHandle m_GLFWHandle{int(m_nWindowWidth), int(m_nWindowHeight),
                            "glTF Viewer",
                            m_OutputPath.empty()}; // show the window only if m_OutputPath is empty

};
